package fact;

public class Fact {


    class Audi implements Cars {

        @Override
        public void drive() {
            System.out.println("drive Audi");
        }
    }

    class Bmv implements Cars {

        @Override
        public void drive() {
            System.out.println("drive Bmv");
        }
    }

    class Mercedes implements Cars {

        @Override
        public void drive() {
            System.out.println("drive Mercedes");
        }
    }

    class Galeon implements Boats {

        @Override
        public void drive() {
            System.out.println("drive Galeon");
        }
    }

    class Caravella implements Boats {

        @Override
        public void drive() {
            System.out.println("drive Caravella");
        }
    }

    class Fregat implements Boats {

        @Override
        public void drive() {
            System.out.println("drive Fregat");
        }
    }

   public class CarFactory implements Factory {
        public Cars createCar(int k) {
            switch (k) {
                case 1:
                    return new Audi();
                case 2:
                    return new Bmv();
                case 3:
                    return new Mercedes();
                default:
                    return null;
            }
        }

       @Override
       public Boats createBoat(int k) {
           return null;
       }
   }

    class BoatFactory implements Factory {
        public Boats createBoat(int k) {
            switch (k) {
                case 1:
                    return new Galeon();
                case 2:
                    return new Caravella();
                case 3:
                    return new Fregat();
                default:
                    return null;
            }
        }

        @Override
        public Cars createCar(int k) {
            return null;
        }
    }

  abstract class FactoryFactory {
        Factory createFactory(int n) {

            switch (n) {
                case 1:
                    return new CarFactory();
                case 2:
                    return new BoatFactory();
                default:
                    return null;
            }
        }
    }

}


