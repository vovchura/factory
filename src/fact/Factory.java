package fact;

public interface Factory {
    Cars createCar(int k);
    Boats createBoat(int k);
}

